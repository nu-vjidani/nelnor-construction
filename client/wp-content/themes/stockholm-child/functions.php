<?php

// enqueue the child theme stylesheet

Function wp_schools_enqueue_scripts() {
wp_register_style( 'childstyle', get_stylesheet_directory_uri() . '/style.css'  );
wp_enqueue_style( 'childstyle' );
}
add_action( 'wp_enqueue_scripts', 'wp_schools_enqueue_scripts', 11);

//require("UserPortal/functions.php");

function custom_login() { 
	wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/style-login.css' );
}
add_action( 'login_enqueue_scripts', 'custom_login' );

function hide_adminstrator_role( $roles ){
	$user = wp_get_current_user();
	if ( in_array( 'nelnor-staff', (array) $user->roles ) ) {
	   unset( $roles['administrator'] );
	   unset( $roles['nelnor-staff'] );
	} 
	/*
	elseif ( get_current_user_id() == 9 ){
	   unset( $roles['administrator'] );
	}
	*/
	return $roles;
}
add_action('editable_roles', 'hide_adminstrator_role');

function hide_all_agents_subagents( $u_query ) {
	$user = wp_get_current_user();
	if ( in_array( 'nelnor-staff', (array) $user->roles ) ) { 
	    global $wpdb;
	    $u_query->query_where = str_replace(
		'WHERE 1=1', 
		"WHERE 1=1 AND {$wpdb->users}.ID IN (
		    SELECT {$wpdb->usermeta}.user_id FROM $wpdb->usermeta 
		        WHERE {$wpdb->usermeta}.meta_key = '{$wpdb->prefix}capabilities'
		        AND {$wpdb->usermeta}.meta_value NOT LIKE '%administrator%' AND {$wpdb->usermeta}.meta_value NOT LIKE '%nelnor-staff%')", 
		$u_query->query_where
	    );
	}
	/*
	elseif ( get_current_user_id() == 9) { 
	    global $wpdb;
	    $u_query->query_where = str_replace(
		'WHERE 1=1', 
		"WHERE 1=1 AND {$wpdb->users}.ID IN (
		    SELECT {$wpdb->usermeta}.user_id FROM $wpdb->usermeta 
		        WHERE {$wpdb->usermeta}.meta_key = '{$wpdb->prefix}capabilities'
		        AND {$wpdb->usermeta}.meta_value NOT LIKE '%administrator%')", 
		$u_query->query_where
	    );
	}
	*/
}
add_action('pre_user_query','hide_all_agents_subagents');

