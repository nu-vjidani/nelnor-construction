<?php /* Template Name: Sign In */ ?>

<?php
global $portal_error;
get_header();
?>
    <div class="Portal">



<?php if(isset($_SESSION["Portal_user"])){ ?>
    <h2 class="Portal__Title"> Welcome to your client portal.</h2>
    <!--<input class="PortalForm__Button qbutton" name="Portal_Logout" type="button" value="Logout"> -->
    <a class="PortalForm__Button qbutton" href="?logout">Logout</a>
<?php } else { ?>
    <h2 class="Portal__Title"> Sign In</h2>
        <form class="PortalForm" method="POST">
            <?php if($portal_error == true){
                echo "<div class='PortalForm__ErrorContainer'>
                        <h2 class='PortalForm__ErrorMessage'>Sorry but there was something wrong with your login credentials. Please check your username and password before trying again.</h2>
                      </div>";
            } ?>
            <label for="Portal_emailAddress">Email Address</label>
            <input class="PortalForm__Field" id="Portal_emailAddress" name="Portal_emailAddress" type="email">
            <label for="Portal_psw">Password</label>
            <input class="PortalForm__Field"  id="Portal_psw" type="password" name="Portal_psw">

            <input class="PortalForm__Button qbutton" name="Portal_submit" type="submit" value="Login">
        </form>
    </div>

<?php } ?>
    <?php get_footer(); ?>