<?php
/**
 * Created by PhpStorm.
 * User: d.russel
 * Date: 3/2/2018
 * Time: 2:10 PM
 */


function user_portal_enqueue_styles() {
    wp_enqueue_style( 'user-portal-stylesheet', get_stylesheet_directory_uri() . '/UserPortal/style.css','','' );
    wp_enqueue_script( 'user-portal-script', get_stylesheet_directory_uri() . '/UserPortal/script.js',' array(\'jquery\')','' );
}
add_action( 'wp_enqueue_scripts', 'user_portal_enqueue_styles' );



/*
* Creating a function to create our Portal clients CPT
*/

function portal_clients_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => __( 'Portal Clients' ),
        'singular_name'       => __( 'Client' ),
        'all_items'           => __( 'All Clients'),
        'view_item'           => __( 'View Client'),
        'add_new_item'        => __( 'Add New Client'),
        'add_new'             => __( 'Add New'),
        'edit_item'           => __( 'Edit Client'),
        'update_item'         => __( 'Update Client'),
        'search_items'        => __( 'Search Client'),
        'not_found'           => __( 'Not Found'),
        'not_found_in_trash'  => __( 'Not found in Trash'),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'Portal Clients', 'twentythirteen' ),
        'description'         => __( 'Clients who have portal accounts.', 'twentythirteen' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title'),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'capability_type'     => 'page',
    );

    // Registering your Custom Post Type
    register_post_type( 'portalclients', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'portal_clients_post_type', 0 );


/**
 * Add meta box
 *
 */
function portal_clients_meta_boxes( $post ){
    add_meta_box( 'portalclient_email', 'Client Email', 'portal_email_build_meta_box', 'portalclients');
    add_meta_box( 'portalclient_psw', 'Client Password', 'portal_psw_build_meta_box', 'portalclients');
}
add_action( 'add_meta_boxes', 'portal_clients_meta_boxes' );


/**
 * Meta box display callback.
 *
 */
function portal_email_build_meta_box( $post ) {
    // Display code/markup goes here. Don't forget to include nonces!
    $Portal_emailAddress = get_post_meta($post->ID, '_Portal_emailAddress_meta_key', true);
    echo '<input id="Portal_emailAddress" name="Portal_emailAddress" type="email" value="' . $Portal_emailAddress . '">';
}
function portal_psw_build_meta_box( $post ) {
    // Display code/markup goes here. Don't forget to include nonces!
    $Portal_psw = get_post_meta($post->ID, '_Portal_psw_meta_key', true);
    echo '<input id="Portal_psw" name="Portal_psw" type="text" value="' . $Portal_psw . '">';
}

/**
 * Save meta box content.
 */
function portal_save_meta_box( $post_id ) {
    // Save logic goes here. Don't forget to include nonce checks!

    if (array_key_exists('Portal_emailAddress', $_POST)) {
        update_post_meta(
            $post_id,
            '_Portal_emailAddress_meta_key',
            $_POST['Portal_emailAddress']
        );
    }

    if (array_key_exists('Portal_psw', $_POST)) {
        update_post_meta(
            $post_id,
            '_Portal_psw_meta_key',
            $_POST['Portal_psw']
        );
    }
}
add_action( 'save_post', 'portal_save_meta_box');

/**
 * Remove Rev Slider Metabox from Portal Clients CPT
 */
if ( is_admin() ) {

    function remove_revolution_slider_meta_boxes() {
        remove_meta_box( 'mymetabox_revslider_0', 'portalclients', 'normal' );
    }

    add_action( 'do_meta_boxes', 'remove_revolution_slider_meta_boxes' );

}

global $portal_error;
$portal_error = false;
function nelnor_start_session()
{
    global $portal_error;
    // Start the session
    session_start();



    /*user sign in */
    /* On submit button press */
    if(isset($_POST['Portal_submit'])) {
        global $wpdb;

        /*create variables that store the login values entered by the user*/
        $user = $_POST['Portal_emailAddress'];
        $psw = $_POST['Portal_psw'];


        $args = array(
            'post_type' => 'portalclients',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => '_Portal_emailAddress_meta_key',
                    'value' => $user,
                    'compare' => '='
                ),
                array(
                    'key' => '_Portal_psw_meta_key',
                    'value' => $psw,
                    'compare' => '='
                )
            )
        );

        $Portal_query = new WP_Query( $args );


        if( $Portal_query->have_posts()) {
            // Set session variables for username and password
            $_SESSION["Portal_user"] = $user;
            //successful login
            $portal_error = false;

        } else {
            //failed login
            $portal_error = true;

            unset( $_SESSION["Portal_user"]);
        }
    }

    /* On logout button press */
    if(isset($_GET['logout'])){

        unset( $_SESSION["Portal_user"]);
        session_destroy();
        header('Location: '. get_bloginfo() . '/sign-in');
        exit();
    }


}

add_action( 'wp', 'nelnor_start_session');

//callback function for portal login check shortcode
function portal_login_check_shortcodeFunction() {
    echo '<div class="Portal__Menu">';
    echo '<a href="' . get_bloginfo() . '/sign-in">My Account </a>';
    echo '<p> | </p>';
    if(isset($_SESSION["Portal_user"])) {
        echo '<a href="?logout">Sign Out</a>';

    } else {
        echo '<a href="' . get_bloginfo() . '/sign-in">Sign In</a>';
    }
    echo '</div>';
}

//register_shortcode
function register_portal_shortcodes() {
    add_shortcode('portal-login-check', 'portal_login_check_shortcodeFunction');
}

//tie into Wordpress's initialization action in order to execute our register_portal_shortcodes() function
add_action( 'init', 'register_portal_shortcodes');